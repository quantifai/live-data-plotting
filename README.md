
# Live tick data Plotting

## Introduction

1. using currBar list to contain the incoming ticks.
2. using filledBars deque to fix the framesize.
3. three timestamps in total:
	- 'previousTime' being used to remove the duplicated ticks.
	- 'timestamp' is the current timestamp of the fresh ticks.
	- 'previousTimeStamp' contains the last candle final tick timestamp.
4.'candleFlag' to check if there is a new candle in the fresh back of ticks we sampled.
5. iterating over the fresh batch of ticks till the (timestamp - previousTimeStamp) satisfies the candle size, using the 'index' to segregate the fresh candle and the old one.
6. oHLC() takes in the parameter currBar which is list of lists containing the current batch of tick info, converts them to DataFrame, operates on it, returns a data frame with a single row of OHLC values of the batch. I am currently keeping all the ticks I have operated on before and redoing the calculation of calculating the OHLC's which is not needed, I am gonna change it to reduce the space and time of the execution of it.
7. clearing and plotting the dynamic candle on every run of animate().
8. clearing and plotting the static candles only when new candle arrives.

## Running

```
usage: plot.py [-h] [-f FRAMESIZE] [-c CANDLESIZE]

on the fly ohlc bar generation

optional arguments:
  -h, --help            show this help message and exit
  -f FRAMESIZE, --frameSize FRAMESIZE
                        specify the size of frame
  -c CANDLESIZE, --candleSize CANDLESIZE
                        specify the size of candles
```
