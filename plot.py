#!/usr/bin/env python3
"""
    :author: Sunil R
    :brief: Live tick to candle plotting with variable resample rate

"""
from influxdb import DataFrameClient
from matplotlib import style
from mpl_finance import candlestick2_ohlc
from threading import Thread

import argparse
import collections
import datetime
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import pandas as pd
import pytz
import matplotlib.pyplot as plt
import time


candleFlag = False
candleSize = None
currBar = []
filledBars = None
flag = False
previousTime = datetime.datetime.utcnow().replace(tzinfo=pytz.UTC) #non naive name
previousTimeStamp = previousTime
temp = pd.DataFrame({'open':[], 'high':[], 'low':[], 'close':[]})
timestamp = previousTime


def collect_args():
    parser = argparse.ArgumentParser(description='on the fly ohlc bar generation')
    parser.add_argument('-f', '--frameSize', type=int, default = 60, help='specify the size of frame')
    parser.add_argument('-c', '--candleSize', type=int, default= 1, help='specify the size of candles')
    args = parser.parse_args()
    return args


def oHLC(temp, currBar):
    dframe = pd.DataFrame(currBar)
    #if not currBar:
    #    return pd.DataFrame({'open':[],'high':[],'low':[],'close':[]})
    if(temp.empty):
        _open = dframe.iloc[0][2]
        high = dframe[2].max()
        low = dframe[2].min()
    else:
        _open = temp.iloc[0]['open']
        high = max(dframe[2].max(), temp.iloc[0]['high'])
        low = min(dframe[2].min(), temp.iloc[0]['low'])
    close = dframe.iloc[-1][2]
    return pd.DataFrame({'open':[_open],'high':[high],'low':[low],'close':[close]})


def animate(i, candleSize, ax2, cli):
    current = datetime.datetime.now()
    
    global candles, flag, candleFlag, currBar, previousTime, timestamp, previousTimeStamp, temp
    
    q = cli.query('select dateTime,ask,bid from tick ORDER BY time DESC LIMIT 100') 
    df = pd.DataFrame(q['tick'])
    df['last'] = (df['ask']+df['bid'])/2
    
    #filtering out the ticks which were in the previous candle
    for x in range (df.shape[0]):
        if(df.iloc[x].name > previousTime): 
            df = df.iloc[x:]
            break
    previousTime = df.iloc[-1].name #stores the timestamp of the last latest aggregated ticks
    if flag == False:
        previousTimeStamp = df.iloc[0].name
        flag = True

    #check if there is a possibility of new candle in the ticks
    if(df.iloc[-1].name - previousTimeStamp > candleSize):
        candleFlag = True
        
    index = 0
    while (index < df.shape[0]) & (timestamp - previousTimeStamp < candleSize):
        timestamp = df.iloc[index].name
        currBar.append(df.iloc[index].tolist()) #need to decrease the Space complexity on this one.
        index += 1

    if not currBar: #if no new bars, skip the computation.
        return
    temp = oHLC(temp, currBar) 
    currBar = []

    if(candleFlag == True):
        filledBars.pop()
        filledBars.extend(temp.values.tolist())
        candles = pd.DataFrame(list(filledBars))
        #ax2.clear()
        #candlestick2_ohlc(ax2,
        #                candles[0].values,
        #                candles[1].values,
        #                candles[2].values,
        #                candles[3].values,
        #                width=0.6,
        #                colorup='g',
        #                colordown='r',
        #                alpha=1)
        previousTimeStamp = df.iloc[index].name
        df = df.iloc[index:]
        for x in range(df.shape[0]):
            currBar.append(df.iloc[x].tolist()) #change the incoming bar to currBar
        temp = temp.iloc[0:0]
        temp = oHLC(temp, currBar)
        filledBars.append(temp.values.tolist())
        candleFlag = False

    #displaying current
    if filledBars:
        filledBars.pop()
    filledBars.extend(temp.values.tolist())
    candles = pd.DataFrame(list(filledBars))
    print(candles)
    #displaying current
    ax2.clear()
    candlestick2_ohlc(ax2,
                        candles[0].values,
                        candles[1].values,
                        candles[2].values,
                        candles[3].values,
                        width=0.6,
                        colorup='g',
                        colordown='r',
                        alpha=1)
    #print(datetime.datetime.now()-current)


def main():
    global frameSize, filledBars
    args = collect_args()
    candleSize = datetime.timedelta(minutes = args.candleSize) # a delta of one minute
    filledBars = collections.deque(maxlen=args.frameSize) #only will show 20 bars at a time 
    fig= plt.figure()
    #ax1 = plt.subplot2grid(shape=(1,4), loc=(0,3), colspan=1)
    #ax2 = plt.subplot2grid(shape=(1,4), loc=(0,0))#, colspan=3, sharey = ax1)
    #ax2 = plt.figure()
    plt.xlabel('timeframe')
    plt.ylabel('pips')
    plt.title('Live Candle Charting')
    ax2 = plt.subplot2grid(shape=(1,1), loc=(0,0))
    # ax1.set_xlim([0.5,2.5])
    # ax1.set_ylim([0.5,2.5])
    # ax2.set_xlim([0.5,2.5])
    # ax2.set_ylim([0.5,2.5])
    cli = DataFrameClient(host='localhost',port=8086,database='algotrader')
    ani = animation.FuncAnimation(fig, animate, interval=10, fargs=(candleSize, ax2, cli, ))
    plt.show()
    

if __name__ == '__main__':
    main()

